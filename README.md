<h1>MCord - What is it?</h1>
<p>This is my try to combine the Minecraft in-game chat with a Discord channel.</p>

<h1>How to Install?</h1>

<p>On the same way as any other plugin too, you will need:</p>
<ul>
<li>A Minecraft server with Bukkit API e.g. <a href="https://www.spigotmc.org/">Spigot</a></li>
<li>A way to get the MCord.jar into the <code>&lt;server_root&gt;/plugins</code> folder</li>
<li>The MCord.jar -> <a href="https://gitlab.com/Yannik_Sc/MCord/tree/master/Build">here</a></li>
</ul>

<h1>Configure the Plugin:</h1>
<p>Start the server for the first time or create the required files by your own (little tip: just start the server, its easier). Now edit the config as you need it.</p>

<h3>Explain the config variables:</h3>
<ul>
<li><code>email</code> - required: the email address of your bot.</li>
<li><code>password</code> - required: the password of your bot.</li>

<li><code>server</code> - alternative: the default Discord server for cross-chatting</li>
<li><code>channel</code> - alternative: the default Discord channel for cross-chatting</li>
<li><code>server</code> - alternative: the default server for cross-chatting</li>

<li><code>sendDiscordMessage</code> - configures: set to false if you don't want the bot to send a message into the Discord channel</li>
<li><code>discordMessage</code> - configures: the message that the bot send into to cross-chat channel</li>

<li><code>discordAdminUID</code> - recommended: Discord user id of the user who can summon the bot</li>
</ul>

<h1>Commands:</h1>
<ul>
<li><code>!MCord WriteHere</code> - Discord command: sets the cross-chat channel</li>
</ul>

<h1>Planed:</h1>
<ul>
<li>a way to ignore the Discord chat in Minecraft</li>
<li>convert the bot user to a real Discord bot</li>
</ul>
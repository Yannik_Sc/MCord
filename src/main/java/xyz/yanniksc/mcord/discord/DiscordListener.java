package xyz.yanniksc.mcord.discord;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.util.concurrent.FutureCallback;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.Channel;
import de.btobastian.javacord.entities.Server;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.listener.message.MessageCreateListener;
import xyz.yanniksc.mcord.MCord;

public class DiscordListener {
	
	public static void Connect(String email, String password) {
		MCord.api = Javacord.getApi(email, password);
		
		
		
		MCord.api.connect(new FutureCallback<DiscordAPI>() {
			
			public void onSuccess(DiscordAPI result) {
				Collection<Server> servers = MCord.api.getServers();
				Server mesa = null;
				
				for(Server s : servers) {
					if(s.getName().equals(MCord.defServer)) {
						mesa = s;
						break;
					}
				}
				
				Collection<Channel> chats = mesa.getChannels();
				
				for(Channel c : chats) {
					if(c.getName().equals(MCord.defChannel)) {
						MCord.channel = c;
						break;
					}
				}
				
				if(MCord.channel != null) {
					if(MCord.sendDiscordMessage) {
						MCord.channel.sendMessage(MCord.discordMessage);
					}
				}
				
				MCord.api.registerListener(new MessageCreateListener() {
					public void onMessageCreate(DiscordAPI api, Message message) {
						if(message.getChannelReceiver() == MCord.channel && message.getAuthor() != api.getYourself()) {
							for(Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(ChatColor.BLUE + "Discord " + ChatColor.WHITE + "<" + message.getAuthor().getName() + "> " + message.getContent());
							}
						}
						
						if(message.getContent().equals("!MCord WriteHere") &&
								(message.getAuthor().getId().equals(MCord.discordAdminUID))) {
							if(MCord.sendDiscordMessage) {
								message.reply(MCord.discordMessage);
							}
							
							MCord.channel = message.getChannelReceiver();
						}
					}
				});
			}
			
			public void onFailure(Throwable t) {
				System.out.println("[MCord] Failed to establish connection to discord");
			}
		});
	}
}

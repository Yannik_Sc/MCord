package xyz.yanniksc.mcord.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import xyz.yanniksc.mcord.MCord;

public class ListenCommand implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			MCord.ignored.remove((Player)sender);
			sender.sendMessage(ChatColor.BLUE + "[MCord] Du ignorierst den Discord Chat nicht mehr");
			return true;
		} else {
			sender.sendMessage("The server can't ignore discord");
			return false;
		}
	}
	
}

package xyz.yanniksc.mcord.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import xyz.yanniksc.mcord.MCord;

public class IgnoreCommand implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			MCord.ignored.put((Player)sender, true);
			sender.sendMessage(ChatColor.BLUE + "[MCord] Du ignorierst nun den Discord Chat");
			return true;
			
		} else {
			sender.sendMessage("The server can't ignore discord");
			return false;
		}
	}
}

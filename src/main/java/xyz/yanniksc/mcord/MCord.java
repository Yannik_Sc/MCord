package xyz.yanniksc.mcord;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.entities.Channel;
import xyz.yanniksc.mcord.discord.DiscordListener;

public class MCord extends JavaPlugin {
	public static DiscordAPI api;
	public static Channel channel;

	public static String defServer = "htwhtwrhrwth";
	public static String defChannel = "fdgsfdgsdfgsfdgsdfgsdfg";
	
	public static boolean sendDiscordMessage = true;
	public static String discordMessage = "";
	public static String discordAdminUID = "";
	
	public static Map<Player, Boolean> ignored = new HashMap<Player, Boolean>();
	
	public void onEnable() {
		FileConfiguration config = this.getConfig();
		
		config.options().copyDefaults(true);
		config.options().header("Minecraft < - > Discord Chat Plugin aka. MCord\nFor help visit http://yannik-sc.xyz/MCord");
		
		config.addDefault("email", "example@example.com");
		config.addDefault("password", "12345");

		config.addDefault("server", "MyServer");
		config.addDefault("channel", "ingamechat-channel");

		config.addDefault("sendDiscordMessage", false);
		config.addDefault("discordMessage", "Cross-Chat aktiv in diesem Channel");
		config.addDefault("discordAdminUID", "012345678987654321");
		
		saveConfig();
		
		String email = config.getString("email");
		String password = config.getString("password");

		defServer = config.getString("server");
		defChannel = config.getString("channel");
		
		sendDiscordMessage = config.getBoolean("sendDiscordMessage");
		discordMessage = config.getString("discordMessage");
		discordAdminUID = config.getString("discordAdminUID");
		
		DiscordListener.Connect(email, password);
		
//		getCommand("ignoreDiscord").setExecutor(new IgnoreCommand());
//		getCommand("listenDiscord").setExecutor(new ListenCommand());
		
		getServer().getPluginManager().registerEvents(new Listener() {
			@EventHandler
			public void onChat(AsyncPlayerChatEvent e) {
				if(MCord.channel != null) {
					MCord.channel.sendMessage("<" + e.getPlayer().getName() + "> " + e.getMessage());
				}
			}
		}, this);
	}
	
	public void onDisable() {
		System.out.println(ChatColor.RED + "[MCord] A reload can/will destroy this plugin D:>");
	}
	
}
